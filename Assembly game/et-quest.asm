jmp main

;Nome: Aríthissa Vitória da Silva  	NUSP: 11366889
;Nome: João Pedro de Andrade    	NUSP: 11831071
;Nome: Lucas Silva Neves 		NUSP: 10288633

main:

	call ApagaTela				; (abaixo imprime-se a tela de início)

	loadn R1, #InicioLinha0		; Endereco onde comeca a primeira linha do cenario!!
	loadn R2, #1536  			; cor Azul!
	call ImprimeTela   			; Rotina de Impresao de Cenario na Tela Inteira
	
	call AguardaEnter
	
	call ApagaTela				; (Abaixo começa a primeira tela de jogo)

	call parte1

	halt

;********************************************************
;                       IMPRIME TELA
;********************************************************	

ImprimeTela: 	;  Rotina de Impresao de Cenario na Tela Inteira
		;  r1 = endereco onde comeca a primeira linha do Cenario
		;  r2 = cor do Cenario para ser impresso

	push r0	; joga o valor de r0 na pilha para uso posterior
	push r1	; joga o valor de r1 na pilha para uso posterior
	push r2	; joga o valor de r2 na pilha para uso posterior
	push r3	; joga o valor de r3 na pilha para uso posterior
	push r4	; joga o valor de r4 na pilha para uso posterior
	push r5	; joga o valor de r5 na pilha para uso posterior
	push r6	; joga o valor de r6 na pilha para uso posterior

	loadn R0, #0  		; posicao inicial tem que ser o comeco da tela!
	loadn R3, #40  	; Incremento da posicao da tela!
	loadn R4, #41  	; incremento do ponteiro das linhas da tela
	loadn R5, #1200 	; Limite da tela!
	loadn R6, #tela0Linha0	; Endereco onde comeca a primeira linha do cenario!!
	
   ImprimeTela_Loop:
		call ImprimeStr
		add r0, r0, r3  	; incrementaposicao para a segunda linha na tela -->  r0 = R0 + 40
		add r1, r1, r4  	; incrementa o ponteiro para o comeco da proxima linha na memoria (40 + 1 porcausa do /0 !!) --> r1 = r1 + 41
		add r6, r6, r4  	; incrementa o ponteiro para o comeco da proxima linha na memoria (40 + 1 porcausa do /0 !!) --> r1 = r1 + 41
		cmp r0, r5			; Compara r0 com 1200
		jne ImprimeTela_Loop		; Enquanto r0 < 1200

	pop r6				; Resgata os valores salvos na pilha
	pop r5
	pop r4
	pop r3
	pop r2
	pop r1
	pop r0
	rts
				
;---------------------

;---------------------------	
;********************************************************
;                   IMPRIME STRING
;********************************************************
	
ImprimeStr:	
;  Rotina de Impresao de Mensagens:    r0 = Posicao da tela que o primeiro caractere da mensagem sera' impresso
;  r1 = endereco onde comeca a mensagem
;  r2 = cor da mensagem.   Obs: a mensagem sera' impressa ate' encontrar "/0"

	push r0	; joga o valor de r0 na pilha para uso posterior
	push r1	; joga o valor de r0 na pilha para uso posterior
	push r2	; joga o valor de r0 na pilha para uso posterior
	push r3	; joga o valor de r0 na pilha para uso posterior
	push r4	; joga o valor de r0 na pilha para uso posterior
	push r5	; joga o valor de r0 na pilha para uso posterior
	push r6	; joga o valor de r0 na pilha para uso posterior
	
	
	loadn r3, #'\0'	; Criterio de parada
	loadn r5, #' '		; Espaco em Branco

   ImprimeStr_Loop:	
		loadi r4, r1
		cmp r4, r3		; If (Char == \0)  vai Embora
		jeq ImprimeStr_Sai
		cmp r4, r5		; If (Char == ' ')  Pula outchar do espaco para nao apagar outros caracteres
		jeq ImprimeStr_Skip
		add r4, r2, r4	; Soma a Cor
		outchar r4, r0	; Imprime o caractere na tela
		storei r6, r4
   ImprimeStr_Skip:
		inc r0			; Incrementa a posicao na tela
		inc r1			; Incrementa o ponteiro da String
		inc r6			; Incrementa o ponteiro da String da Tela 0
		jmp ImprimeStr_Loop
	
   ImprimeStr_Sai:	
	pop r6	; Resgata os valores dos registradores utilizados na Subrotina da Pilha
	pop r5
	pop r4
	pop r3
	pop r2
	pop r1
	pop r0
	rts
	

;------------------------	
;********************************************************
;                       APAGA TELA
;********************************************************
ApagaTela:
	push r0
	push r1
	
	loadn r0, #1200		; apaga as 1200 posicoes da Tela
	loadn r1, #' '			; com "espaco"
	
	ApagaTela_Loop:		; label for(r0=1200;r0>0;r0--)
		dec r0
		outchar r1, r0
		jnz ApagaTela_Loop
 
	pop r1
	pop r0
	rts	
	

	
;********************************************************
;                       AGUARDA ENTER
;********************************************************

;r6: Leitura do Caractere
;r7: valor ASCII do ENTER

AguardaEnter:
    inchar r6
    loadn r7, #13
    cmp r7, r6
        jne AguardaEnter
    rts

 
   
;********************************************************
;                       AGUARDA OPCAO
;********************************************************

;r6: Leitura do Caractere
;r7: valor ASCII do numero 1
;r5: valor ASCII do numero 2

AguardaOpcao1:
    inchar r6
    loadn r7, #49	; Opcao 1 (explica)
    loadn r5, #50	; Opcao 2 (bora direto)
    cmp r7, r6
        jeq parte1
    cmp r5, r6
        jeq perguntas
        jne AguardaOpcao1 ; Se nao apertar 1 ou 2 continua aqui
    rts

AguardaOpcao2:
    inchar r6
    loadn r7, #49	; Opcao 1 (explica)
    loadn r5, #50	; Opcao 2 (bora direto)
    cmp r7, r6
        jeq parte2
    cmp r5, r6
        jeq perguntas
        jne AguardaOpcao2 ; Se nao apertar 1 ou 2 continua aqui
    rts

AguardaOpcao3:
    inchar r6
    loadn r7, #49	; Opcao 1 (explica)
    loadn r5, #50	; Opcao 2 (bora direto)
    cmp r7, r6
        jeq parte3
    cmp r5, r6
        jeq perguntas
        jne AguardaOpcao3 ; Se nao apertar 1 ou 2 continua aqui
    rts

AguardaOpcao4:
    inchar r6
    loadn r7, #49	; Opcao 1 (explica)
    loadn r5, #50	; Opcao 2 (bora direto)
    cmp r7, r6
        jeq parte4
    cmp r5, r6
        jeq perguntas
        jne AguardaOpcao4 ; Se nao apertar 1 ou 2 continua aqui
    rts

AguardaOpcao5:
    inchar r6
    loadn r7, #49	; Opcao 1 (explica)
    loadn r5, #50	; Opcao 2 (bora direto)
    cmp r7, r6
        jeq parte5
    cmp r5, r6
        jeq perguntas
        jne AguardaOpcao5 ; Se nao apertar 1 ou 2 continua aqui
    rts

AguardaOpcao6:
    inchar r6
    loadn r7, #49	; Opcao 1 (explica)
    loadn r5, #50	; Opcao 2 (bora direto)
    cmp r7, r6
        jeq parte6
    cmp r5, r6
        jeq perguntas
        jne AguardaOpcao6 ; Se nao apertar 1 ou 2 continua aqui
    rts

AguardaOpcao7:
    inchar r6
    loadn r7, #49	; Opcao 1 (explica)
    loadn r5, #50	; Opcao 2 (bora direto)
    cmp r7, r6
        jeq parte7
    cmp r5, r6
        jeq perguntas
        jne AguardaOpcao7 ; Se nao apertar 1 ou 2 continua aqui
    rts

AguardaOpcao8:         ; EasterEgg
    inchar r6
    loadn r7, #49	; Opcao 1 (modo normal)
    loadn r5, #50	; Opcao 2 (modo wtf)
    cmp r7, r6
        jeq perguntas
    cmp r5, r6
	jeq perguntas
        ;jeq perguntaswtf					;; HABILITA AQUI BIXO
    jne AguardaOpcao8	; Se nao apertar 1 ou 2 continua aqui
    rts



;********************************************************
;                       PARTES (historinha)
;******************************************************** 

parte1:
	call ApagaTela
	loadn R1, #tela1Linha0			; Endereco onde comeca a primeira linha do cenario!!
	loadn R2, #000   			; cor Branca!
	call ImprimeTela
	call AguardaOpcao2 
	rts

parte2:
	call ApagaTela
	loadn R1, #tela2Linha0			; Endereco onde comeca a primeira linha do cenario!!
	loadn R2, #000   			; cor branca
	call ImprimeTela
	call AguardaOpcao3
	rts

parte3:
	call ApagaTela
	loadn R1, #tela3Linha0			; Endereco onde comeca a primeira linha do cenario!!
	loadn R2, #000   			; cor branca
	call ImprimeTela
	call AguardaOpcao4 
	rts

parte4:
	call ApagaTela
	loadn R1, #tela4Linha0			; Endereco onde comeca a primeira linha do cenario!!
	loadn R2, #000   			; cor branca
	call ImprimeTela
	call AguardaOpcao5 
	rts

parte5:
	call ApagaTela
	loadn R1, #tela5Linha0			; Endereco onde comeca a primeira linha do cenario!!
	loadn R2, #000   			; cor branca
	call ImprimeTela
	call AguardaOpcao6 
	rts

parte6:
	call ApagaTela
	loadn R1, #tela6Linha0			; Endereco onde comeca a primeira linha do cenario!!
	loadn R2, #000   			; cor branca
	call ImprimeTela
	call AguardaOpcao7 
	rts

parte7:
	call ApagaTela
	loadn R1, #tela7Linha0			; Endereco onde comeca a primeira linha do cenario!!
	loadn R2, #000   			; cor branca
	call ImprimeTela
	call AguardaOpcao8 
	rts



;********************************************************
;                 Perguntas (aparelho)
;******************************************************** 

perguntas:
	call ApagaTela
	loadn R1, #NitronoLinha0			; Endereco onde comeca a primeira linha do cenario!!
	loadn R2, #2816   				; cor amarela!
	call ImprimeTela

	call AguardaEnter

	loadn r4, #48
	loadn r5, #48

	jmp pergunta1			
	rts

perguntaswtf:
	call ApagaTela
	loadn R1, #NitronoLinha0		; Endereco onde comeca a primeira linha do cenario!!
	loadn R2, #2816   			; cor amarela!
	call ImprimeTela

	call AguardaEnter

	;call wtf1				;;; HABILITA AQUI BIXO
	rts



;********************************************************
;                     Erros e Acertos
;********************************************************



acerto:
	push r2
	push FR

	inc r4
	loadn r2, #58
	cmp r2, r4
		jeq vitoria		 

	loadn r3, #9

	pop FR
	pop r2

	rts

erro:


	push r2
	push FR

	inc r5
	loadn r2, #51
	cmp r2, r5
		jeq derrota		 

	loadn r3, #9

	pop FR
	pop r2

	rts


;********************************************************
;                Verificação de Teclas
;********************************************************

verifica1:		; verifica se a resposta foi ou não válida
    loadn r7, #49      ; caractere equivalente a '1'
    cmp r6, r7
        ceq erro
        
    loadn r7, #50      ; caractere equivalente a '2'
    cmp r6, r7
        ceq erro
    
    loadn r7, #51      ; caractere equivalente a '3'
    cmp r6, r7
        ceq erro

    loadn r7, #52      ; caractere equivalente a '4'
    cmp r6, r7
        ceq erro

    rts



;********************************************************
;                Perguntas e Respostas
;********************************************************

pergunta1:
	call ApagaTela
	loadn R1, #p1Linha0			; Endereco onde comeca a primeira linha do cenario!!
	loadn R2, #000   			; cor branca
	call ImprimeTela
	loadn r1, #1132
	outchar r4, r1
	loadn r1, #1157
	outchar r5, r1

	loadn r3, #0
	call resposta1 
	rts

resposta1:
    inchar r6
    loadn r7, #49	; numero que corresponde ao acerto (favor verificar)

    cmp r7, r6
        ceq acerto
    cmp r7, r6
        cne verifica1

    loadn r2, #9
    cmp r3, r2
        jeq pergunta2

    jmp resposta1	; se tudo deu errado até aqui, ele tenta de novo
    rts

pergunta2:
	call ApagaTela
	loadn R1, #p2Linha0			; Endereco onde comeca a primeira linha do cenario!!
	loadn R2, #000   			; cor branca
	call ImprimeTela
	loadn r1, #1132
	outchar r4, r1
	loadn r1, #1157
	outchar r5, r1

	loadn r3, #0
	call resposta2 
	rts

resposta2:
    inchar r6
    loadn r7, #51	; numero que corresponde ao acerto (favor verificar)

    loadn r2, #9

    cmp r7, r6
        ceq acerto
        cne verifica1

    cmp r3, r2
        jeq pergunta3
    
    jmp resposta2	; se tudo deu errado até aqui, ele tenta de novo
    rts

pergunta3:
	call ApagaTela
	loadn R1, #p3Linha0			; Endereco onde comeca a primeira linha do cenario!!
	loadn R2, #000   			; cor branca
	call ImprimeTela
	loadn r1, #1132
	outchar r4, r1
	loadn r1, #1157
	outchar r5, r1

	loadn r3, #0
	call resposta3 
	rts

resposta3:
    inchar r6
    loadn r7, #50	; numero que corresponde ao acerto (favor verificar)

    loadn r2, #9

    cmp r7, r6
        ceq acerto
        cne verifica1

    cmp r3, r2
        jeq pergunta4
    
    jmp resposta3	; se tudo deu errado até aqui, ele tenta de novo
    rts


pergunta4:
	call ApagaTela
	loadn R1, #p4Linha0			; Endereco onde comeca a primeira linha do cenario!!
	loadn R2, #000   			; cor branca
	call ImprimeTela
	loadn r1, #1132
	outchar r4, r1
	loadn r1, #1157
	outchar r5, r1

	loadn r3, #0
	call resposta4 
	rts

resposta4:
    inchar r6
    loadn r7, #50	; numero que corresponde ao acerto (favor verificar)

    loadn r2, #9

    cmp r7, r6
        ceq acerto
        cne verifica1

    cmp r3, r2
        jeq pergunta5
    
    jmp resposta4
    rts

pergunta5:
	call ApagaTela
	loadn R1, #p5Linha0			; Endereco onde comeca a primeira linha do cenario!!
	loadn R2, #000   			; cor branca
	call ImprimeTela
	loadn r1, #1132
	outchar r4, r1
	loadn r1, #1157
	outchar r5, r1

	loadn r3, #0
	call resposta5 
	rts

resposta5:
    inchar r6
    loadn r7, #51	; numero que corresponde ao acerto (favor verificar)
    loadn r2, #9

    cmp r7, r6
        ceq acerto
        cne verifica1

    cmp r3, r2
        jeq pergunta6
    
    jmp resposta5
    rts

pergunta6:
	call ApagaTela
	loadn R1, #p6Linha0			; Endereco onde comeca a primeira linha do cenario!!
	loadn R2, #000   			; cor branca
	call ImprimeTela
	loadn r1, #1132
	outchar r4, r1
	loadn r1, #1157
	outchar r5, r1

	loadn r3, #0
	call resposta6 
	rts

resposta6:
    inchar r6
    loadn r7, #52	; numero que corresponde ao acerto (favor verificar)
    loadn r2, #9

    cmp r7, r6
        ceq acerto
        cne verifica1

    cmp r3, r2
        jeq pergunta7
    
    jmp resposta6
    rts

pergunta7:
	call ApagaTela
	loadn R1, #p7Linha0			; Endereco onde comeca a primeira linha do cenario!!
	loadn R2, #000   			; cor branca
	call ImprimeTela
	loadn r1, #1132
	outchar r4, r1
	loadn r1, #1157
	outchar r5, r1

	loadn r3, #0
	call resposta7 
	rts

resposta7:
    inchar r6
    loadn r7, #51	; numero que corresponde ao acerto (favor verificar)
    loadn r2, #9

    cmp r7, r6
        ceq acerto
        cne verifica1

    cmp r3, r2
        jeq pergunta8
    
    jmp resposta7
    rts

pergunta8:
	call ApagaTela
	loadn R1, #p8Linha0			; Endereco onde comeca a primeira linha do cenario!!
	loadn R2, #000   			; cor branca
	call ImprimeTela
	loadn r1, #1132
	outchar r4, r1
	loadn r1, #1157
	outchar r5, r1

	loadn r3, #0
	call resposta8 
	rts

resposta8:
    inchar r6
    loadn r7, #49	; numero que corresponde ao acerto (favor verificar)
    loadn r2, #9

    cmp r7, r6
        ceq acerto
        cne verifica1

    cmp r3, r2
        jeq pergunta9
    
    jmp resposta8
    rts

pergunta9:
	call ApagaTela
	loadn R1, #p9Linha0			; Endereco onde comeca a primeira linha do cenario!!
	loadn R2, #000   			; cor branca
	call ImprimeTela
	loadn r1, #1132
	outchar r4, r1
	loadn r1, #1157
	outchar r5, r1

	loadn r3, #0
	call resposta9 
	rts

resposta9:
    inchar r6
    loadn r7, #52	; numero que corresponde ao acerto (favor verificar)
    loadn r2, #9

    cmp r7, r6
        ceq acerto
        cne verifica1

    cmp r3, r2
        jeq pergunta10
    
    jmp resposta9
    rts

pergunta10:
	call ApagaTela
	loadn R1, #p10Linha0			; Endereco onde comeca a primeira linha do cenario!!
	loadn R2, #000   			; cor branca
	call ImprimeTela
	loadn r1, #1132
	outchar r4, r1
	loadn r1, #1157
	outchar r5, r1

	loadn r3, #0
	call resposta10
	rts

resposta10:
    inchar r6
    loadn r7, #50	; numero que corresponde ao acerto (favor verificar)
    loadn r2, #9

    cmp r7, r6
        ceq acerto
        cne verifica1

    cmp r3, r2
        jeq pergunta11
    
    jmp resposta10
    rts

pergunta11:
	call ApagaTela
	loadn R1, #p11Linha0			; Endereco onde comeca a primeira linha do cenario!!
	loadn R2, #000   			; cor branca
	call ImprimeTela
	loadn r1, #1132
	outchar r4, r1
	loadn r1, #1157
	outchar r5, r1

	loadn r3, #0
	call resposta11
	rts

resposta11:
    inchar r6
    loadn r7, #49	; numero que corresponde ao acerto (favor verificar)
    loadn r2, #9

    cmp r7, r6
        ceq acerto
        cne verifica1

    cmp r3, r2
        jeq pergunta12
    
    jmp resposta11
    rts

pergunta12:
	call ApagaTela
	loadn R1, #p12Linha0			; Endereco onde comeca a primeira linha do cenario!!
	loadn R2, #000   			; cor branca
	call ImprimeTela
	loadn r1, #1132
	outchar r4, r1
	loadn r1, #1157
	outchar r5, r1

	call resposta12
	rts

resposta12:
    inchar r6

    loadn r7, #49	; numero que corresponde ao acerto
    cmp r7, r6
        ceq acerto

    loadn r7, #50	; numero que corresponde ao acerto
    cmp r7, r6
        ceq acerto

    loadn r7, #51	; numero que corresponde ao acerto
    cmp r7, r6
        ceq acerto

    loadn r7, #52	; numero que corresponde ao acerto
    cmp r7, r6
        ceq acerto

    jmp resposta12	; se tudo deu errado até aqui, ele tenta de novo



;********************************************************
;                        Finais
;********************************************************

vitoria:
	call ApagaTela
	loadn R1, #VitoriaLinha0		; Endereco onde comeca a primeira linha do cenario!!
	loadn R2, #512   			; cor Verde
	call ImprimeTela

	call AguardaEnter

	jmp main
	rts

derrota:
	call ApagaTela
	loadn R1, #DerrotaLinha0		; Endereco onde comeca a primeira linha do cenario!!
	loadn R2, #2304   			; cor Vermelha
	call ImprimeTela

	call AguardaEnter

	jmp main
	rts

;********************************************************
;                        Telas
;********************************************************


InicioLinha0  : string "                                        "
InicioLinha1  : string "                                        "
InicioLinha2  : string "                                        "
InicioLinha3  : string "                                        "
InicioLinha4  : string "                                        "
InicioLinha5  : string "      _____________    _______          "
InicioLinha6  : string "     / ____/__  __/    TTo oTT          "                  
InicioLinha7  : string "    / /___   / /        |_=_/  |Y/      "                
InicioLinha8  : string "   / ____/  / /          |  |  //       "                     
InicioLinha9  : string "  / /___   / /          /    |//        "                         
InicioLinha10 : string " /_____/__/_/ __   ____/______|_____ ___"                 
InicioLinha11 : string "     / __  / / /  / / ____// ___/__  __/"               
InicioLinha12 : string "    / / / / / /  / / /___ / /___  / /   "                   
InicioLinha13 : string "   / /_/ / / /  / / ____//___  / / /    "            
InicioLinha14 : string "  /      |/ /__/ / /___ ____/ / / /     "  
InicioLinha15 : string " /_____|__|_____/_____//_____/ /_/      "                  
InicioLinha16 : string "                                        "                                                         
InicioLinha17 : string "                                        "                                                                   
InicioLinha18 : string "                                        "                                                               
InicioLinha19 : string "           [Pressione Enter]            "                                                          
InicioLinha20 : string "                                        "
InicioLinha21 : string "                                        "
InicioLinha22 : string "                                        "
InicioLinha23 : string "                                        "
InicioLinha24 : string "                                        "
InicioLinha25 : string "                                        "
InicioLinha26 : string "                                        "
InicioLinha27 : string "                                        "
InicioLinha28 : string "                                   :)   "
InicioLinha29 : string "                                        "

tela0Linha0  : string "                                        "
tela0Linha1  : string "                                        "
tela0Linha2  : string "                                        "
tela0Linha3  : string "                                        "
tela0Linha4  : string "                                        "
tela0Linha5  : string "                                        "
tela0Linha6  : string "                                        "
tela0Linha7  : string "                                        "
tela0Linha8  : string "                                        "
tela0Linha9  : string "                                        "
tela0Linha10 : string "                                        "
tela0Linha11 : string "                                        "
tela0Linha12 : string "                                        "
tela0Linha13 : string "                                        "
tela0Linha14 : string "                                        "
tela0Linha15 : string "                                        "
tela0Linha16 : string "                                        "
tela0Linha17 : string "                                        "
tela0Linha18 : string "                                        "
tela0Linha19 : string "                                        "
tela0Linha20 : string "                                        "
tela0Linha21 : string "                                        "
tela0Linha22 : string "                                        "
tela0Linha23 : string "                                        "
tela0Linha24 : string "                                        "
tela0Linha25 : string "                                        "
tela0Linha26 : string "                                        "
tela0Linha27 : string "                                        "
tela0Linha28 : string "                                        "
tela0Linha29 : string "                                        "	

tela1Linha0  : string "                                        "
tela1Linha1  : string "                                        "
tela1Linha2  : string "      O-oi!                             "
tela1Linha3  : string "      Meu nome eh X AE A-12.            "
tela1Linha4  : string "      Eu estou perdido.                 "
tela1Linha5  : string "                                        "
tela1Linha6  : string "      Voce pode me ajudar a chegar      " 
tela1Linha7  : string "      em casa?                          "
tela1Linha8  : string "                                        "
tela1Linha9  : string "      Voce:                             "
tela1Linha10 : string "        1. O que aconteceu?             "
tela1Linha11 : string "        2. Certo, direto ao ponto.      "
tela1Linha12 : string "                                        "
tela1Linha13 : string "                                        "
tela1Linha14 : string "                                        "
tela1Linha15 : string "                                        "
tela1Linha16 : string "                                        "
tela1Linha17 : string "                                        "
tela1Linha18 : string "                                        "
tela1Linha19 : string "                                        "
tela1Linha20 : string "                                        "
tela1Linha21 : string "                                        "
tela1Linha22 : string "                                        "
tela1Linha23 : string "                                        "
tela1Linha24 : string "                                        "
tela1Linha25 : string "                                        "
tela1Linha26 : string "                                        "
tela1Linha27 : string "                                        "
tela1Linha28 : string "                                        "
tela1Linha29 : string "                                        "

tela2Linha0  : string "                                        "
tela2Linha1  : string "                                        "
tela2Linha2  : string "     X AE A-12:                         "
tela2Linha3  : string "                                        "
tela2Linha4  : string "     Bom, tudo comecou                  "
tela2Linha5  : string "     ontem a noite. Minha mae,          "
tela2Linha6  : string "     Ellen, e meu pai, Mosca, estavam   "
tela2Linha7  : string "     na sala da nossa casa, que fica    "
tela2Linha8  : string "     no planeta TOI 700 d.              "
tela2Linha9  : string "                                        "
tela2Linha10 : string "     De repente surgiu um clarao no     "
tela2Linha11 : string "     ceu, eles sairam para ver o que    "
tela2Linha12 : string "     era e me disseram para ir pro      "
tela2Linha13 : string "     meu quarto e aguardar.             "
tela2Linha14 : string "                                        "
tela2Linha15 : string "     Como estavam demorando muito       "
tela2Linha16 : string "     pra voltar, eu fui atras deles     "
tela2Linha17 : string "     para ver o que tinha acontecido.   "
tela2Linha18 : string "                                        "
tela2Linha19 : string "     Assim que pisei fora de casa,      "
tela2Linha20 : string "     tudo ficou preto. E acordei aqui,  "
tela2Linha21 : string "     nesse planeta desconhecido,        "
tela2Linha22 : string "     dormindo dentro de uma nave        "
tela2Linha23 : string "     espacial.                          "
tela2Linha24 : string "                                        "
tela2Linha25 : string "     Voce:                              "
tela2Linha26 : string "       1. Bem-vindo a Terra.            "
tela2Linha27 : string "       2. Nao diga mais nada, eu ajudo! "
tela2Linha28 : string "                                        "
tela2Linha29 : string "                                        "

tela3Linha0  : string "                                        "
tela3Linha1  : string "                                        "
tela3Linha2  : string "     X AE A-12:                         "
tela3Linha3  : string "                                        "
tela3Linha4  : string "     Muito obrigado.                    "
tela3Linha5  : string "     Eh um planeta muito bonito.        "
tela3Linha6  : string "     Mas preciso voltar pra casa.       "
tela3Linha7  : string "     E, sobre o seu idioma,             "
tela3Linha8  : string "     uso um software cerebral           "
tela3Linha9  : string "     de traducao automatica.            "
tela3Linha10 : string "                                        "
tela3Linha11 : string "     Voce:                              "
tela3Linha12 : string "       1. Ahh, que legal.               "
tela3Linha13 : string "          E como posso te ajudar?       "
tela3Linha14 : string "       2. Next, next, next...           "
tela3Linha15 : string "                                        "
tela3Linha16 : string "                                        "
tela3Linha17 : string "                                        "
tela3Linha18 : string "                                        " 
tela3Linha19 : string "                                        "
tela3Linha20 : string "                                        "
tela3Linha21 : string "                                        "
tela3Linha22 : string "                                        "
tela3Linha23 : string "                                        "
tela3Linha24 : string "                                        "
tela3Linha25 : string "                                        "
tela3Linha26 : string "                                        "
tela3Linha27 : string "                                        "
tela3Linha28 : string "                                        "
tela3Linha29 : string "                                        "
 
tela4Linha0  : string "                                        "
tela4Linha2  : string "                                        "
tela4Linha3  : string "     X AE A-12:                         "
tela4Linha4  : string "                                        "
tela4Linha5  : string "     A minha nave esta logo ali do      "
tela4Linha6  : string "     lado e contem um sistema de        "
tela4Linha7  : string "     localizacao interespecial que me   "
tela4Linha8  : string "     mostra a rota de volta para casa.  "
tela4Linha9  : string "                                        "
tela4Linha10 : string "     Mas o combustivel acabou.          "
tela4Linha11 : string "     Preciso conseguir mais.            "
tela4Linha12 : string "                                        "
tela4Linha13 : string "     Voce:                              "
tela4Linha14 : string "       1. Gasolina comum ou aditivada?  "
tela4Linha15 : string "       2. Eu resolvo, lanse a braba.    "
tela4Linha16 : string "                                        "
tela4Linha17 : string "                                        "
tela4Linha18 : string "                                        "
tela4Linha19 : string "                                        "
tela4Linha20 : string "                                        "
tela4Linha21 : string "                                        "
tela4Linha22 : string "                                        "
tela4Linha23 : string "                                        "
tela4Linha24 : string "                                        "
tela4Linha25 : string "                                        "
tela4Linha26 : string "                                        "
tela4Linha27 : string "                                        "
tela4Linha28 : string "                                        "
tela4Linha29 : string "                                        "

tela5Linha0  : string "                                        "
tela5Linha1  : string "                                        "
tela5Linha2  : string "     X AE A-12:                         "
tela5Linha3  : string "                                        "
tela5Linha4  : string "     Nenhum dos dois... a gente usa     "
tela5Linha5  : string "     nitrono, um composto quimico       "
tela5Linha6  : string "     com componentes carbonicos e       "
tela5Linha7  : string "     nitrogenados.                      "
tela5Linha8  : string "                                        "
tela5Linha9  : string "     A minha nave consegue puxar os     "
tela5Linha10 : string "     elementos do ar e preparar esse    "
tela5Linha11 : string "     composto. Porem, ha um problema.   "
tela5Linha12 : string "                                        "
tela5Linha13 : string "     Voce:                              "
tela5Linha14 : string "      1. Qual?                          "
tela5Linha15 : string "      2. Me leve ate la.                "
tela5Linha16 : string "                                        "
tela5Linha17 : string "                                        "
tela5Linha18 : string "                                        " 
tela5Linha19 : string "                                        " 
tela5Linha20 : string "                                        " 
tela5Linha21 : string "                                        "
tela5Linha22 : string "                                        "
tela5Linha23 : string "                                        "
tela5Linha24 : string "                                        "
tela5Linha25 : string "                                        "
tela5Linha26 : string "                                        "
tela5Linha27 : string "                                        "
tela5Linha28 : string "                                        "
tela5Linha29 : string "                                        "

tela6Linha0  : string "                                        "
tela6Linha1  : string "                                        "
tela6Linha2  : string "     X AE A-12:                         "
tela6Linha3  : string "                                        "
tela6Linha4  : string "     O aparelho preparador de nitrono   "
tela6Linha5  : string "     nao usa bateria nem pilha. Ele e   "
tela6Linha6  : string "     carregado por conhecimento.        "
tela6Linha7  : string "                                        "
tela6Linha8  : string "     Entao, para ele poder ser          "
tela6Linha9  : string "     recarregado e preparar meu         "
tela6Linha10 : string "     combustivel de volta para casa,    "
tela6Linha11 : string "     precisamos responder algumas       "
tela6Linha12 : string "     perguntas.                         "
tela6Linha13 : string "                                        "
tela6Linha14 : string "     Voce:                              "
tela6Linha15 : string "       1. Quantas perguntas?            "
tela6Linha16 : string "       2. Eh o suficiente, vamos la.    "
tela6Linha17 : string "                                        "
tela6Linha18 : string "                                        "
tela6Linha19 : string "                                        "
tela6Linha20 : string "                                        "
tela6Linha21 : string "                                        "
tela6Linha22 : string "                                        "
tela6Linha23 : string "                                        "
tela6Linha24 : string "                                        "
tela6Linha25 : string "                                        "
tela6Linha26 : string "                                        "
tela6Linha27 : string "                                        "
tela6Linha28 : string "                                        "
tela6Linha29 : string "                                        "

tela7Linha0  : string "                                        "
tela7Linha1  : string "                                        "
tela7Linha2  : string "     X AE A-12:                         "
tela7Linha3  : string "                                        "
tela7Linha4  : string "     10 perguntas corretas bastarao e   "
tela7Linha5  : string "     o preparador conseguira produzir   "
tela7Linha6  : string "     nitrono suficiente para a minha    "
tela7Linha7  : string "     volta pra casa.                    "
tela7Linha8  : string "                                        "
tela7Linha9  : string "     So tome cuidado: se voce errar 3   "
tela7Linha10 : string "     respostas, o aparelho deixara de   "
tela7Linha11 : string "     funcionar.                         "
tela7Linha12 : string "                                        "
tela7Linha13 : string "     Confio em voce.                    "
tela7Linha14 : string "                                        "
tela7Linha15 : string "                                        "
tela7Linha16 : string "     Voce:                              "
tela7Linha17 : string "       1. Deixa comigo.                 "
tela7Linha18 : string "       2. Espero que tenha modo facil.  " ; easteregg: modo insano
tela7Linha19 : string "                                        " 
tela7Linha20 : string "                                        "
tela7Linha21 : string "                                        "
tela7Linha22 : string "                                        "
tela7Linha23 : string "                                        "
tela7Linha24 : string "                                        "
tela7Linha25 : string "                                        "
tela7Linha26 : string "                                        "
tela7Linha27 : string "                                        "
tela7Linha28 : string "                                        "
tela7Linha29 : string "                                        "

NitronoLinha0  : string "                                        "
NitronoLinha1  : string "                                        "
NitronoLinha2  : string "     Boa tarde! Sou o aparelho          "
NitronoLinha3  : string "     preparador de nitrono e tenho      "
NitronoLinha4  : string "     algumas perguntas pra voce.        "
NitronoLinha5  : string "                                        "
NitronoLinha6  : string "     Cuidado, se errar 3 perguntas eu   "
NitronoLinha7  : string "     sou desativado.                    "
NitronoLinha8  : string "                                        "
NitronoLinha9  : string "     Serao perguntas                    "
NitronoLinha10 : string "     sobre absolutamente quaisquer      "
NitronoLinha11 : string "     temas: conhecimentos gerais,       "
NitronoLinha12 : string "     inuteis e aleatorios.              "
NitronoLinha13 : string "                                        "
NitronoLinha14 : string "        [Pressione Enter para]          "
NitronoLinha15 : string "        [     continuar      ]          "
NitronoLinha16 : string "                                        "
NitronoLinha17 : string "                                        "
NitronoLinha18 : string "                                        "
NitronoLinha19 : string "                                        "
NitronoLinha20 : string "                                        "
NitronoLinha21 : string "                                        "
NitronoLinha22 : string "                                        "
NitronoLinha23 : string "                                        "
NitronoLinha24 : string "                                        "
NitronoLinha25 : string "                                        "
NitronoLinha26 : string "                                        "
NitronoLinha27 : string "                                        "
NitronoLinha28 : string "                                        "
NitronoLinha29 : string "                                        "

p1Linha0  : string "                                        "
p1Linha1  : string "                                        "
p1Linha2  : string "   Quais o menor e o maior pais do      " ; colocar no wtf
p1Linha3  : string " planeta Terra?                         "
p1Linha4  : string "                                        "
p1Linha5  : string "     1. Vaticano e Russia               "
p1Linha6  : string "     2. Nauru e China                   "
p1Linha7  : string "     3. Monaco e Canada                 "
p1Linha8  : string "     4. Malta e Estados Unidos          "
p1Linha9  : string "                                        "
p1Linha10 : string "                                        "
p1Linha11 : string "                                        "
p1Linha12 : string "                                        "
p1Linha13 : string "                                        "
p1Linha14 : string "                                        "
p1Linha15 : string "                                        "
p1Linha16 : string "                                        "
p1Linha17 : string "                                        "
p1Linha18 : string "                                        "
p1Linha19 : string "                                        "
p1Linha20 : string "                                        "
p1Linha21 : string "                                        "
p1Linha22 : string "                                        "
p1Linha23 : string "                                        "
p1Linha24 : string "                                        "
p1Linha25 : string "                                        "
p1Linha26 : string "                                        "    
p1Linha27 : string "                                        "
p1Linha28 : string "  Acertos:                    Erros:    "
p1Linha29 : string "                                        "

p2Linha0  : string "                                        "
p2Linha1  : string "                                        "
p2Linha2  : string "   Quem pintou Guernica?                "
p2Linha3  : string "                                        "
p2Linha4  : string "                                        "
p2Linha5  : string "     1. Salvador Dali                   "
p2Linha6  : string "     2. Diego Riveira                   "
p2Linha7  : string "     3. Pablo Picasso                   "
p2Linha8  : string "     4. Tarsila do Amaral               "
p2Linha9  : string "                                        "
p2Linha10 : string "                                        "
p2Linha11 : string "                                        "
p2Linha12 : string "                                        "
p2Linha13 : string "                                        "
p2Linha14 : string "                                        "
p2Linha15 : string "                                        "
p2Linha16 : string "                                        "
p2Linha17 : string "                                        "
p2Linha18 : string "                                        "
p2Linha19 : string "                                        "
p2Linha20 : string "                                        "
p2Linha21 : string "                                        "
p2Linha22 : string "                                        "
p2Linha23 : string "                                        "
p2Linha24 : string "                                        "
p2Linha25 : string "                                        "
p2Linha26 : string "                                        "
p2Linha27 : string "                                        "
p2Linha28 : string "  Acertos:                    Erros:    "
p2Linha29 : string "                                        "

p3Linha0  : string "                                        "
p3Linha1  : string "                                        "
p3Linha2  : string "   Quanto tempo a luz do Sol demora pra "
p3Linha3  : string " Chegar ao planeta Terra?               "
p3Linha4  : string "                                        "
p3Linha5  : string "     1. 12 minutos                      "
p3Linha6  : string "     2. 8 minutos                       "
p3Linha7  : string "     3. Alguns segundos                 "
p3Linha8  : string "     4. O tempo eh relativo             " ; acrescentar esse nos wtf também
p3Linha9  : string "                                        "
p3Linha10 : string "                                        "
p3Linha11 : string "                                        "
p3Linha12 : string "                                        "
p3Linha13 : string "                                        "
p3Linha14 : string "                                        "
p3Linha15 : string "                                        "
p3Linha16 : string "                                        "
p3Linha17 : string "                                        "
p3Linha18 : string "                                        "
p3Linha19 : string "                                        "
p3Linha20 : string "                                        "
p3Linha21 : string "                                        "
p3Linha22 : string "                                        "
p3Linha23 : string "                                        "
p3Linha24 : string "                                        "
p3Linha25 : string "                                        "
p3Linha26 : string "                                        "
p3Linha27 : string "                                        "
p3Linha28 : string "  Acertos:                    Erros:    "
p3Linha29 : string "                                        "

p4Linha0  : string "                                        "
p4Linha1  : string "                                        "
p4Linha2  : string "   Em que periodo da historia humana    "
p4Linha3  : string " o fogo foi descoberto?                 "
p4Linha4  : string "                                        "
p4Linha5  : string "     1. Neolitico                       "
p4Linha6  : string "     2. Paleolitico                     "
p4Linha7  : string "     3. Mesozoico                       "
p4Linha8  : string "     4. Idade Media                     "
p4Linha9  : string "                                        "
p4Linha10 : string "                                        "
p4Linha11 : string "                                        "
p4Linha12 : string "                                        "
p4Linha13 : string "                                        "
p4Linha14 : string "                                        "
p4Linha15 : string "                                        "
p4Linha16 : string "                                        "
p4Linha17 : string "                                        "
p4Linha18 : string "                                        "
p4Linha19 : string "                                        "
p4Linha20 : string "                                        "
p4Linha21 : string "                                        "
p4Linha22 : string "                                        "
p4Linha23 : string "                                        "
p4Linha24 : string "                                        "
p4Linha25 : string "                                        "
p4Linha26 : string "                                        "
p4Linha27 : string "                                        "
p4Linha28 : string "  Acertos:                    Erros:    "
p4Linha29 : string "                                        "

p5Linha0  : string "                                        "
p5Linha1  : string "                                        "
p5Linha2  : string "   Qual a Velocidade da luz no vacuo?   "  ;colocar no wtf sem a especificação
p5Linha3  : string "                                        "
p5Linha4  : string "                                        "
p5Linha5  : string "     1. 300 000 000 m/s                 "
p5Linha6  : string "     2. 300 000 000 km/s                "
p5Linha7  : string "     3. 299 792 458 m/s                 "
p5Linha8  : string "     4. 301 135 235 m/s                 "
p5Linha9  : string "                                        "
p5Linha10 : string "                                        "
p5Linha11 : string "                                        "
p5Linha12 : string "                                        "
p5Linha13 : string "                                        "
p5Linha14 : string "                                        "
p5Linha15 : string "                                        "
p5Linha16 : string "                                        "
p5Linha17 : string "                                        "
p5Linha18 : string "                                        "
p5Linha19 : string "                                        "
p5Linha20 : string "                                        "
p5Linha21 : string "                                        "
p5Linha22 : string "                                        "
p5Linha23 : string "                                        "
p5Linha24 : string "                                        "
p5Linha25 : string "                                        "
p5Linha26 : string "                                        "
p5Linha27 : string "                                        "
p5Linha28 : string "  Acertos:                    Erros:    "
p5Linha29 : string "                                        "

p6Linha0  : string "                                        "
p6Linha1  : string "                                        "
p6Linha2  : string "   Qual o maior arquipelago da Terra?   " 
p6Linha3  : string "                                        " 
p6Linha4  : string "                                        "
p6Linha5  : string "     1. Filipinas                       "
p6Linha6  : string "     2. Bahamas                         "
p6Linha7  : string "     3. Maldivas                        "
p6Linha8  : string "     4. Indonesia                       "
p6Linha9  : string "                                        "
p6Linha10 : string "                                        "
p6Linha11 : string "                                        "
p6Linha12 : string "                                        "
p6Linha13 : string "                                        "
p6Linha14 : string "                                        "
p6Linha15 : string "                                        "
p6Linha16 : string "                                        "
p6Linha17 : string "                                        "
p6Linha18 : string "                                        "
p6Linha19 : string "                                        "
p6Linha20 : string "                                        "
p6Linha21 : string "                                        "
p6Linha22 : string "                                        "
p6Linha23 : string "                                        "
p6Linha24 : string "                                        "
p6Linha25 : string "                                        "
p6Linha26 : string "                                        "
p6Linha27 : string "                                        "
p6Linha28 : string "  Acertos:                    Erros:    "
p6Linha29 : string "                                        "

p7Linha0  : string "                                        "
p7Linha1  : string "                                        "
p7Linha2  : string "   Que lider mundial ficou conhecida    "
p7Linha3  : string " como Dama de Ferro?                    "
p7Linha4  : string "                                        "
p7Linha5  : string "     1. Dilma Rousseff                  "
p7Linha6  : string "     2. Hillary Clinton                 "
p7Linha7  : string "     3. Margaret Thatcher               "
p7Linha8  : string "     4. Elizabeth II                    "
p7Linha9  : string "                                        "
p7Linha10 : string "                                        "
p7Linha11 : string "                                        "
p7Linha12 : string "                                        "
p7Linha13 : string "                                        "
p7Linha14 : string "                                        "
p7Linha15 : string "                                        "
p7Linha16 : string "                                        "
p7Linha17 : string "                                        "
p7Linha18 : string "                                        "
p7Linha19 : string "                                        "
p7Linha20 : string "                                        "
p7Linha21 : string "                                        "
p7Linha22 : string "                                        "
p7Linha23 : string "                                        "
p7Linha24 : string "                                        "
p7Linha25 : string "                                        "
p7Linha26 : string "                                        "
p7Linha27 : string "                                        "
p7Linha28 : string "  Acertos:                    Erros:    "
p7Linha29 : string "                                        "

p8Linha0  : string "                                        "
p8Linha1  : string "                                        "
p8Linha2  : string "   Quem foi o autor de O Principe?      "
p8Linha3  : string "                                        "
p8Linha4  : string "                                        "
p8Linha5  : string "     1. Maquiavel                       "
p8Linha6  : string "     2. Montesquiel                     "
p8Linha7  : string "     3. Thomas Hobbes                   "
p8Linha8  : string "     4. Rousseau                        "
p8Linha9  : string "                                        "
p8Linha10 : string "                                        "
p8Linha11 : string "                                        "
p8Linha12 : string "                                        "
p8Linha13 : string "                                        "
p8Linha14 : string "                                        "
p8Linha15 : string "                                        "
p8Linha16 : string "                                        "
p8Linha17 : string "                                        "
p8Linha18 : string "                                        "
p8Linha19 : string "                                        "
p8Linha20 : string "                                        "
p8Linha21 : string "                                        "
p8Linha22 : string "                                        "
p8Linha23 : string "                                        "
p8Linha24 : string "                                        "
p8Linha25 : string "                                        "
p8Linha26 : string "                                        "
p8Linha27 : string "                                        "
p8Linha28 : string "  Acertos:                    Erros:    "
p8Linha29 : string "                                        "

p9Linha0  : string "                                        "
p9Linha1  : string "                                        "
p9Linha2  : string "   De quem eh a frase Penso, logo       "
p9Linha3  : string " existo ?                               "
p9Linha4  : string "                                        "
p9Linha5  : string "     1. Che Guevara                     "
p9Linha6  : string "     2. Galileu Galilei                 "
p9Linha7  : string "     3. Socrates                        "
p9Linha8  : string "     4. Platao                          "
p9Linha9  : string "                                        "
p9Linha10 : string "                                        "
p9Linha11 : string "                                        "
p9Linha12 : string "                                        "
p9Linha13 : string "                                        "
p9Linha14 : string "                                        "
p9Linha15 : string "                                        "
p9Linha16 : string "                                        "
p9Linha17 : string "                                        "
p9Linha18 : string "                                        "
p9Linha19 : string "                                        "
p9Linha20 : string "                                        "
p9Linha21 : string "                                        "
p9Linha22 : string "                                        "
p9Linha23 : string "                                        "
p9Linha24 : string "                                        "
p9Linha25 : string "                                        "
p9Linha26 : string "                                        "
p9Linha27 : string "                                        "
p9Linha28 : string "  Acertos:                    Erros:    "
p9Linha29 : string "                                        "

p10Linha0  : string "                                        "
p10Linha1  : string "                                        "
p10Linha2  : string "   Qual foi a primeira organizacao nao  "
p10Linha3  : string " governamental a enviar um satelite ao  "
p10Linha4  : string " espaco?                                "
p10Linha5  : string "     1. Space X                         "
p10Linha6  : string "     2. Space Services Inc.             " ;
p10Linha7  : string "     3. NASA                            "
p10Linha8  : string "     4. POCKOCMOC                       "
p10Linha9  : string "                                        "
p10Linha10 : string "                                        "
p10Linha11 : string "                                        "
p10Linha12 : string "                                        "
p10Linha13 : string "                                        "
p10Linha14 : string "                                        "
p10Linha15 : string "                                        "
p10Linha16 : string "                                        "
p10Linha17 : string "                                        "
p10Linha18 : string "                                        "
p10Linha19 : string "                                        "
p10Linha20 : string "                                        "
p10Linha21 : string "                                        "
p10Linha22 : string "                                        "
p10Linha23 : string "                                        "
p10Linha24 : string "                                        "
p10Linha25 : string "                                        "
p10Linha26 : string "                                        "
p10Linha27 : string "                                        "
p10Linha28 : string "  Acertos:                    Erros:    "
p10Linha29 : string "                                        "

p11Linha0  : string "                                        "
p11Linha1  : string "                                        "
p11Linha2  : string "   Qual a velocidade do som no vacuo?   "
p11Linha3  : string "                                        "
p11Linha4  : string "                                        "
p11Linha5  : string "     1. 0 m/s                           "
p11Linha6  : string "     2. 337 m/s                         "
p11Linha7  : string "     3. 1450 m/s                        "
p11Linha8  : string "     4. Nenhuma das Anteriores          "
p11Linha9  : string "                                        "
p11Linha10 : string "                                        "
p11Linha11 : string "                                        "
p11Linha12 : string "                                        "
p11Linha13 : string "                                        "
p11Linha14 : string "                                        "
p11Linha15 : string "                                        "
p11Linha16 : string "                                        "
p11Linha17 : string "                                        "
p11Linha18 : string "                                        "
p11Linha19 : string "                                        "
p11Linha20 : string "                                        "
p11Linha21 : string "                                        "
p11Linha22 : string "                                        "
p11Linha23 : string "                                        "
p11Linha24 : string "                                        "
p11Linha25 : string "                                        "
p11Linha26 : string "                                        "
p11Linha27 : string "                                        "
p11Linha28 : string "  Acertos:                    Erros:    "
p11Linha29 : string "                                        "

p12Linha0  : string "                                        "
p12Linha1  : string "                                        "
p12Linha2  : string "   Qual o melhor aparelho preparador    "
p12Linha3  : string " de nitrono de todo o universo?         "
p12Linha4  : string "                                        "
p12Linha5  : string "     1. Eh voce mesmo                   "
p12Linha6  : string "     2. Eh voce mesmo                   "
p12Linha7  : string "     3. Eh voce mesmo                   "
p12Linha8  : string "     4. Eh voce mesmo                   "
p12Linha9  : string "                                        "
p12Linha10 : string "                                        "
p12Linha11 : string "                                        "
p12Linha12 : string "                                        "
p12Linha13 : string "                                        "
p12Linha14 : string "                                        "
p12Linha15 : string "                                        "
p12Linha16 : string "                                        "
p12Linha17 : string "                                        "
p12Linha18 : string "                                        "
p12Linha19 : string "                                        "
p12Linha20 : string "                                        "
p12Linha21 : string "                                        "
p12Linha22 : string "                                        "
p12Linha23 : string "                                        "
p12Linha24 : string "                                        "
p12Linha25 : string "                                        "
p12Linha26 : string "                                        "
p12Linha27 : string "                                        "
p12Linha28 : string "  Acertos:                    Erros:    "
p12Linha29 : string "                                        "


VitoriaLinha0  : string "                                        "
VitoriaLinha1  : string "                                        "
VitoriaLinha2  : string "     X AE A-12:                         "
VitoriaLinha3  : string "                                        "
VitoriaLinha4  : string "     Obrigado por conseguir me ajudar   "
VitoriaLinha5  : string "     a voltar pra casa, foi um prazer   "
VitoriaLinha6  : string "     viver essa jornada com voce! Os    "
VitoriaLinha7  : string "     seres humanos sao mesmo            "
VitoriaLinha8  : string "     maravilhosos.                      "
VitoriaLinha9  : string "                                        "
VitoriaLinha10 : string "                  [FIM]                 " 
VitoriaLinha11 : string "                                        "
VitoriaLinha12 : string "                                        "
VitoriaLinha13 : string "                                        "
VitoriaLinha14 : string "                                        "
VitoriaLinha15 : string "                                        "
VitoriaLinha16 : string "                                        "
VitoriaLinha17 : string "                                        "
VitoriaLinha18 : string "                                        "
VitoriaLinha19 : string "                                        "
VitoriaLinha20 : string "                                        "
VitoriaLinha21 : string "                                        "
VitoriaLinha22 : string "                                        "
VitoriaLinha23 : string "                                        "
VitoriaLinha24 : string "                                        "
VitoriaLinha25 : string "                                        "
VitoriaLinha26 : string "                                        "
VitoriaLinha27 : string "                                        "
VitoriaLinha28 : string "                                        "
VitoriaLinha29 : string "                                        "

DerrotaLinha0  : string "                                        "
DerrotaLinha1  : string "                                        "
DerrotaLinha2  : string "     X AE A-12:                         "
DerrotaLinha3  : string "                                        "
DerrotaLinha4  : string "     Agora estou preso na Terra para    "
DerrotaLinha5  : string "     sempre com essa especie imbecil    "
DerrotaLinha6  : string "     da qual voce faz parte.            "
DerrotaLinha7  : string "     Satisfeito, idiota?                "
DerrotaLinha8  : string "         _______________________        "
DerrotaLinha9  : string "        |Voce errou 3 respostas |       "
DerrotaLinha10 : string "        | Pressione Enter para  |       "
DerrotaLinha11 : string "        |____tentar_novamente___|       "
DerrotaLinha12 : string "                                        "
DerrotaLinha13 : string "                 [FIM]                  "
DerrotaLinha14 : string "                                        "
DerrotaLinha15 : string "                                        "
DerrotaLinha16 : string "                                        "
DerrotaLinha17 : string "                                        "
DerrotaLinha18 : string "                                        "
DerrotaLinha18 : string "                                        "
DerrotaLinha20 : string "                                        "
DerrotaLinha21 : string "                                        "
DerrotaLinha22 : string "                                        "
DerrotaLinha23 : string "                                        "
DerrotaLinha24 : string "                                        "
DerrotaLinha25 : string "                                        "
DerrotaLinha26 : string "                                        "
DerrotaLinha27 : string "                                        "
DerrotaLinha28 : string "                                        "
DerrotaLinha29 : string "                                        "
