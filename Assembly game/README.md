**Digital Computers Organization (SSC0511)**

This folder contains files of a game project made on assembly for the ICMC-USP processor (https://github.com/simoesusp/Processador-ICMC).

The game is a simple Text-Adventure with a set of multiple answer questions written in pt-br.
Demonstration video: https://drive.google.com/file/d/1MNBVtVOkllLS7-ihfIrOb43CBx4O2yWq/view?usp=sharing

People involved:

    Aríthissa Vitória da Silva  NUSP: 11366889
    João Pedro de Andrade       NUSP: 11831071
    Lucas Silva Neves           NUSP: 10288633

    
**New processor function implementation**

As provided by our OrgComp classes, students should modify the ICMC-USP processor in any way in order to better understand concepts of low level programming and hardware features. Those changes lies here: https://gitlab.com/arithissa/SSC0511-Trabalho1/-/tree/master



